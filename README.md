##ArahEmu##
---
New version of our *Guild Wars 2* server -  now written in Java!

Requirements:

- Java 8 w/ Lambda support (JDK 1.8.0 or above)
- Maven (3.2.1 or above)
- An IDE of your own choice (IntelliJ IDEA preferred)