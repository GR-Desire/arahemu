@echo off
cd .\..
cmd /c "mvn clean install"
mkdir build
copy framework\target\arahemu-framework-1.0-SNAPSHOT.jar build\arahemu-framework-1.0-SNAPSHOT.jar
copy game-server\target\arahemu-game-server-1.0-SNAPSHOT.jar build\arahemu-game-server-1.0-SNAPSHOT.jar
copy login-server\target\arahemu-login-server-1.0-SNAPSHOT.jar build\arahemu-login-server-1.0-SNAPSHOT.jar
echo.
echo Done!
pause>nul