@echo off
cd .\..
rd framework\target /S /Q
rd game-server\target /S /Q
rd login-server\target /S /Q
rd build /S /Q
echo Done!
pause>nul