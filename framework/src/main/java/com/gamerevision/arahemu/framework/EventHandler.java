package com.gamerevision.arahemu.framework;

public interface EventHandler<T extends Event> {
    void trigger(T event);
}
