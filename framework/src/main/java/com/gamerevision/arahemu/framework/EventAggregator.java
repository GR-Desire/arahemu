package com.gamerevision.arahemu.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventAggregator {
    private Map<Class<?>, List<EventHandler<?>>> eventHandlerMap;

    public EventAggregator() {
        this.eventHandlerMap = new HashMap<Class<?>, List<EventHandler<?>>>();
    }

    public EventAggregator register(Registrable registrable) {
        registrable.register(this);

        return this;
    }

    public <T extends Event> EventAggregator register(Class<T> eventClass, EventHandler<T> eventHandler) {
        if (!eventHandlerMap.containsKey(eventClass)) {
            eventHandlerMap.put(eventClass, new ArrayList<EventHandler<?>>());
        }

        eventHandlerMap.get(eventClass).add(eventHandler);

        return this;
    }

    public EventAggregator trigger(Event event) {
        Class<?> eventClass = event.getClass();

        if (eventHandlerMap.containsKey(eventClass)) {
            List<EventHandler<?>> eventHandlers = eventHandlerMap.get(eventClass);

            for (EventHandler<?> eventHandler : eventHandlers) {
                ((EventHandler<Event>)eventHandler).trigger(event);
            }
        }

        return this;
    }
}
