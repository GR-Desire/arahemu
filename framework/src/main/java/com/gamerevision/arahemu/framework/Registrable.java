package com.gamerevision.arahemu.framework;

public interface Registrable {
    void register(EventAggregator eventAggregator);
}
