package com.gamerevision.arahemu.login;

import com.gamerevision.arahemu.framework.Event;
import com.gamerevision.arahemu.framework.EventAggregator;

/**
 * Test code for EventAggregator. First steps.
 */
public class Main {
    private static class TestEvent implements Event {
        public TestEvent(int test) {
            this.test = test;
        }

        public int test;
    }

    public static void main(String[] args) {
        EventAggregator eventAggregator = new EventAggregator();
        eventAggregator.register(TestEvent.class, (event) -> {
            System.out.println(event.test);
        });

        eventAggregator.trigger(new TestEvent(100));
    }
}
